"""Tests for validating Computes. """
import pytest


@pytest.mark.asyncio
async def test_list_instances(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.list_instances(
        ctx,
        availability_domain=value,
        capacity_reservation_id=value,
        compute_cluster_id=value,
        compartment_id=value,
        display_name=value,
        limit=value,
        page=value,
        sort_by=value,
        sort_order=value,
        lifecycle_state=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_launch_instance(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.launch_instance(
        ctx,
        opc_retry_token=value,
        agent_config=value,
        availability_config=value,
        availability_domain=value,
        capacity_reservation_id=value,
        compartment_id=value,
        compute_cluster_id=value,
        create_vnic_details=value,
        dedicated_vm_host_id=value,
        defined_tags=value,
        display_name=value,
        extended_metadata=value,
        fault_domain=value,
        freeform_tags=value,
        hostname_label=value,
        image_id=value,
        instance_options=value,
        ipxe_script=value,
        is_pv_encryption_in_transit_enabled=value,
        launch_mode=value,
        launch_options=value,
        metadata=value,
        platform_config=value,
        preemptible_instance_config=value,
        shape=value,
        shape_config=value,
        source_details=value,
        subnet_id=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_instance(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.get_instance(ctx, instance_id=value)
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_update_instance(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.update_instance(
        ctx,
        instance_id=value,
        opc_retry_token=value,
        if_match=value,
        agent_config=value,
        availability_config=value,
        capacity_reservation_id=value,
        defined_tags=value,
        display_name=value,
        extended_metadata=value,
        fault_domain=value,
        freeform_tags=value,
        instance_options=value,
        launch_options=value,
        metadata=value,
        shape=value,
        shape_config=value,
        time_maintenance_reboot_due=value,
        update_operation_constraint=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_instance_action(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.instance_action(
        ctx,
        instance_id=value,
        action=value,
        opc_retry_token=value,
        if_match=value,
        action_type=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_terminate_instance(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.terminate_instance(
        ctx, instance_id=value, if_match=value, preserve_boot_volume=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_accept_shielded_integrity_policy(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.accept_shielded_integrity_policy(
        ctx,
        instance_id=value,
        opc_request_id=value,
        if_match=value,
        opc_retry_token=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_change_instance_compartment(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.change_instance_compartment(
        ctx,
        if_match=value,
        instance_id=value,
        opc_request_id=value,
        opc_retry_token=value,
        compartment_id=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_instance_default_credentials(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.get_instance_default_credentials(
        ctx, instance_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_list_instance_devices(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.list_instance_devices(
        ctx,
        is_available=value,
        instance_id=value,
        limit=value,
        page=value,
        opc_request_id=value,
        sort_by=value,
        sort_order=value,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_windows_instance_initial_credentials(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.get_windows_instance_initial_credentials(
        ctx, instance_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_instance_maintenance_reboot(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.get_instance_maintenance_reboot(
        ctx, instance_id=value, opc_request_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations


@pytest.mark.asyncio
async def test_get_measured_boot_report(hub, ctx):
    r"""
    **Test function**
    """

    # TODO: replace call param values as necessary
    ret = await hub.tool.oci.compute.get_measured_boot_report(
        ctx, instance_id=value, opc_request_id=value
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    ret["ret"]
    # TODO: Add manual validations
